#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os

from mutagen.id3 import ID3

from music_metadata_stuffer.identifying.audio_identifier import \
    audio_identifier, AcoustidError_NoTrackFound
from music_metadata_stuffer.fetching.metadata_fetcher import \
    metadata_fetcher
from music_metadata_stuffer.stuffing.metadata_stuffer_mixin import \
    metadata_stuffer_mixin


__all__ = ['metadata_stuffer']


DISABLED = True


class metadata_stuffer(metadata_stuffer_mixin):
    """
        Main class that will manage the different API requests and
        modification of given file
    """
    filepath: str = None
    video_title: str = None
    track_title: str = None
    metadatas: dict = None

    def __init__(self, filepath: str, playlist_title: str,
                 video_title: str, video_id: str):
        self.filepath = filepath
        self.dirpath = filepath.rsplit('/', 1)[0]
        self.tags_editor = ID3(self.filepath)
        self.pl_title = playlist_title
        self.video_title = video_title
        self.video_id = video_id
        self.metadatas = None
        self.output_filepath = None

    def run(self):
        """
            Start trying to fetch metadata and fill the file
            with it if succeed
        """
        if not DISABLED and self.__audio_not_too_long():
            self.__try_stuffing_metadata()
        else:
            self.metadatas = {
                'TIT2': self.video_title,
                'TCON': self.pl_title,
                'TALB': self.pl_title,
                'album_cover': None
            }
            self.stuff_metadatas_on_file()
        self.store_cover_as_tag()
        self.__rename_file()

    def __audio_not_too_long(self) -> bool:
        """
            Check file size is not too long (>15M) to avoid trying to fetch
            metadata on non-music files
            return: (bool) True if file size inferior to 15M
        """
        return os.path.getsize(self.filepath) < 15000000

    def __try_stuffing_metadata(self):
        try:
            self.track_title = audio_identifier(self.filepath,
                                                self.video_title).get_track_title()
        except AcoustidError_NoTrackFound:
            pass
        else:
            self.metadatas = metadata_fetcher(self.track_title).get_metadatas()
            if self.metadatas:
                self.stuff_metadatas_on_file()

    def __rename_file(self):
        """
           Rename mp3 track based on fetch metadatas or provided
           title if could not fetch anything
           return (none)
        """
        if self.metadatas and self.metadatas['TIT2']:
            filename = self.metadatas['TIT2']
        elif self.track_title:
            filename = self.track_title
        else:
            filename = self.video_title
        self.output_filepath = self.dirpath + '/' + filename + '.mp3'
        os.rename(self.filepath, self.output_filepath)


if __name__ in '__main__':
    metadata_stuffer('/home/xibao/.krakeno/downloads/0xfe3ad/audio.mp3',
                     'Playlist Title Bis',
                     'French 79 - Between The Buttons [Live]',
                     'qO-M65IdBnA').run()
