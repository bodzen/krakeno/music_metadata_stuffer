#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
from sentry_python_integration.common import CustomSentryException


class EnvVarError(CustomSentryException):
    def build_error_msg(self, varname: str):
        return f'"{varname} is not a defined env var."'


def get_env_var(varname: str) -> str:
    try:
        return os.environ[varname]
    except KeyError:
        if not os.getenv('UNITTESTING', None):
            raise EnvVarError(varname)


SPOTIFY_CLIENT_ID = get_env_var('SPOTIFY_CLIENT_ID')
SPOTIFY_CLIENT_SECRET = get_env_var('SPOTIFY_CLIENT_SECRET')

NAPSTER_API_KEY = get_env_var('NAPSTER_API_KEY')

ACOUSTID_API_KEY = get_env_var('ACOUSTID_API_KEY')


# https://help.mp3tag.de/main_tags.html
MP3_TAGS = (
    {'TIT2': "track's title"},
    {'TPE1': "artists' names"},
    {'TALB': "album's name"},
    {'TPE2': "album's artists' names"},
    {'COMM': "track's description"},
    {'TCOM': "composer's name"},
    {'TCON': "track's genre"},
    {'TDRC': "track's release date"},
    {'TLAN': "track's language"},
    {'TLEN': "track's length in second"},
    {'TRCK': "track's number in album"},
    {'TPOS': "Disc Number"}
)
