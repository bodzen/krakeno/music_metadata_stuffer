#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import NoReturn

import requests
from mutagen.id3 import \
    TIT2, TALB, TPE1, TPE2, COMM, TCOM, TCON, TDRC, TLAN, TLEN, TRCK, TPOS, APIC  # noqa

from custom_py_exceptions import SubclassMissingVariableError

from music_metadata_stuffer.config import MP3_TAGS


__all__ = ['metadata_stuffer_mixin']


class metadata_stuffer_mixin:
    """
        Mixin used by main class to actually write metadata to audio file
    """
    def __init_subclass__(cls, *args, **kwargs):
        cls.__check_subclass_alpha(cls)
        super.__init_subclass__(*args, **kwargs)

    def __check_subclass_alpha(subclass) -> NoReturn:
        assert hasattr(subclass, 'filepath'), \
            SubclassMissingVariableError(subclass.__name__, 'filepath')
        assert hasattr(subclass, 'metadatas'), \
            SubclassMissingVariableError(subclass.__name__, 'metadatas')
        pass

    def stuff_metadatas_on_file(self):
        """
            Fill file with fetched metadatas
        """
        for tag_infos in MP3_TAGS:
            for tag_name in tag_infos:
                self.__store_tag_info(tag_name)
        self.tags_editor.save()

    def __store_tag_info(self, tag_name: str):
        """
            Create tag in given mp3 file, using fetch metadatas values
        """
        try:
            if isinstance(self.metadatas[tag_name], dict):
                value = '/'.join(metadatas[tag_name])
            else:
                value = self.metadatas[tag_name]
            if value:
                self.tags_editor[tag_name] = eval(tag_name)(encoding=3, text=value)  # nosec
        except KeyError:
            pass
        except NameError as err:
            raise ImportError(f'Missing tag import: {err.args}')

    def store_cover_as_tag(self):
        """
            Download cover and store it inside tag.
            return: (None)
        """
        fd, mime = self.__get_cover_infos()
        self.tags_editor['APIC'] = APIC(encoding=3, mime=mime,
                                        desc=u'Cover', data=fd.read())
        self.tags_editor.save()
        fd.close()

    def __get_cover_infos(self):
        """
            Download cover and return a file descriptor of it + its mime
            return: cover fd
        """
        if self.metadatas and self.metadatas['album_cover']:
            return self.__download_and_return_cover_as_fd(self.metadatas['album_cover']), \
                self.metadatas['album_cover'].split('.')[-1]
        else:
            url = f'https://img.youtube.com/vi/{self.video_id}/hqdefault.jpg'
            return self.__download_and_return_cover_as_fd(url), 'image/jpg'

    def __download_and_return_cover_as_fd(self, cover_url: str):
        """
            Download and store given cover in a fd before returning it
            return: file descriptor of the downloaded cover
        """
        cover_path = self.dirpath + '/cover.jpg'
        with requests.get(cover_url, stream=True) as r:
            with open(cover_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=81920):
                    f.write(chunk)
        return open(cover_path, 'rb')
