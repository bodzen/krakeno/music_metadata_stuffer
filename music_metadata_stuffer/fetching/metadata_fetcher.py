#!/usr/bin/python3
# -*- coding: utf-8 -*-

from time import sleep

from music_metadata_stuffer.fetching.mixins import \
    napster_api_mixin, \
    NapsterError_TooMuchRequest, \
    NapsterError_TrackNotFound, \
    NapsterError_InvalidCredentials, \
    spotify_api_mixin, \
    SpotifyError_InvalidCredentials, \
    SpotifyError_TooMuchRequest, \
    SpotifyError_TrackNotFound, \
    deezer_api_mixin, DeezerError_TrackNotFound, \
    itunes_api_mixin, ItunesError_TrackNotFound


__all__ = ['metadata_fetcher']


class FailedBasicDataValidation(Exception):
    pass


class metadata_fetcher(napster_api_mixin,
                       spotify_api_mixin,
                       deezer_api_mixin,
                       itunes_api_mixin):
    """
        Main class that will query the different API until video
        title matches or it exhausted all API
    """

    title: str = None
    metadatas: dict = None

    def __init__(self, title: str):
        self.title = title
        self.apis = ('napster', 'spotify', 'itunes', 'deezer', )

    def get_metadatas(self):
        """
            Start trying to fetch metadata and fill the file
            with it if succeed
        """
        for api in self.apis:
            self.__request_api(api)
            if self.metadatas:
                return self.metadatas

    def __request_api(self, api: str):
        """
            Request API only one time even if no matching result
        """
        try:
            self.metadatas = getattr(self, api + '_fetch_metadata')()
            self.__basic_data_validation()
        except (SpotifyError_TrackNotFound, SpotifyError_TooMuchRequest,
                SpotifyError_InvalidCredentials, DeezerError_TrackNotFound,
                ItunesError_TrackNotFound, NapsterError_InvalidCredentials,
                NapsterError_TrackNotFound, NapsterError_TooMuchRequest,
                FailedBasicDataValidation):
            sleep(1)

    def __basic_data_validation(self):
        """
            Validate that the returned data contain relevant title or artist
        """
        if self.metadatas['TIT2'].lower() not in self.title.lower() and \
           self.metadatas['TPE1'].lower() not in self.title.lower():
            self.metadatas = None
            raise FailedBasicDataValidation()


if __name__ in '__main__':
    inst = metadata_fetcher('50 Cent P.I.M.P.')
    try:
        inst.get_metadatas()
        from pprint import pprint as pp
        pp(inst.metadatas)
    except AttributeError:
        print('[-] No metadata found')
