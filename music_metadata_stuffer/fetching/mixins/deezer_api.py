#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import NoReturn

import deezer

from custom_py_exceptions import SubclassMissingVariableError


__all__ = [
    'deezer_api_mixin',
    'DeezerError_TrackNotFound'
]


class DeezerError_TrackNotFound(Exception):
    pass


class deezer_api_mixin:
    DEEZER = None

    def __init_subclass__(cls, *args, **kwargs):
        cls.__check_subclass_beta(cls)
        super().__init_subclass__(*args, **kwargs)

    def __check_subclass_beta(subclass) -> NoReturn:
        assert hasattr(subclass, 'title'), \
            SubclassMissingVariableError(subclass.__name__, 'title')
        pass

    def deezer_fetch_metadata(self) -> dict:
        """
            Request metadata to Deezer API using cropped title
            and return formated data to be used by main subclass
            return: (dict) formated data
        """
        self.__init_deezer()
        if not (data := self.__get_data_from_deezer()):
            raise DeezerError_TrackNotFound()
        return self.__format_deezer_data(data)

    def __init_deezer(self) -> NoReturn:
        """
            Init connection to deezer API
        """
        self.DEEZER = deezer.Client()

    def __get_data_from_deezer(self) -> dict:
        """
            Send request to deezer to fetch music's data
            return: (dict) music data
        """
        try:
            return self.DEEZER.search(self.title)
        # We cannot manage more properly as the package
        # only raise ValueError exception
        except ValueError:
            raise DeezerError_TrackNotFound()

    def __format_deezer_data(self, data) -> dict:
        """
            Formated data following main class needs
            return: (dict) formatted data
        """
        try:
            infos = data[0].asdict()
        except (IndexError):
            raise DeezerError_TrackNotFound()
        return {
            'TIT2': infos['title'],
            'TPE1': infos['artist']['name'],
            'TALB': infos['album']['title'],
            'TPE2': infos['artist']['name'],
            'album_cover': infos['album']['cover_big']
        }
