#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import NoReturn
import requests
import urllib.parse

from custom_py_exceptions import SubclassMissingVariableError
from sentry_python_integration.common import CustomSentryException

from music_metadata_stuffer.config import \
    NAPSTER_API_KEY


__all__ = [
    'napster_api_mixin',
    'NapsterError_TrackNotFound',
    'NapsterError_TooMuchRequest',
    'NapsterError_InvalidCredentials'
]


class NapsterError_TrackNotFound(Exception):
    pass


class NapsterError_TooMuchRequest(CustomSentryException):
    pass


class NapsterError_InvalidCredentials(CustomSentryException):
    pass


class napster_api_mixin:
    napster_base_url = None

    def __init_subclass__(cls, *args, **kwargs):
        cls.__check_subclass_beta(cls)
        super().__init_subclass__(*args, **kwargs)

    def __check_subclass_beta(subclass) -> NoReturn:
        assert hasattr(subclass, 'title'), \
            SubclassMissingVariableError(subclass.__name__, 'title')
        pass

    def napster_fetch_metadata(self) -> dict:
        """
            Request metadata to Napster API using cropped title
            and return formated data to be used by main subclass
            return: (dict) formated data
        """
        self.__init_napster()
        if not (data := self.__get_track_data_from_napster()):
            raise NapsterError_TrackNotFound()
        return self.__format_napster_data(data)

    def __init_napster(self) -> NoReturn:
        """
            Init connection to napster API
        """
        self.napster_base_url = 'https://api.napster.com/v2.2/search' \
            f'?apikey={NAPSTER_API_KEY}'

    def __get_track_data_from_napster(self) -> dict:
        """
            Try to fetched track data from napster and return it as dict
            return: (dict) track data or None if nothing to fetch
        """
        escaped_title = urllib.parse.quote(self.title)
        url = self.napster_base_url + f'&query={escaped_title}' \
            '?&type=track&per_type_limit=1'
        return self.__get_data_from_napster(url)

    def __get_data_from_napster(self, url: str) -> dict:
        """
            Send request to napster to fetch data based on given url
            return: (dict) music data
        """
        try:
            resp = requests.get(url)
            if resp.status_code == 401:
                raise NapsterError_InvalidCredentials()
            return resp.json()
        except requests.exceptions.HTTPError as err:
            if err.status_code == 429:
                raise NapsterError_TooMuchRequest()
            raise NapsterError_TrackNotFound()

    def __format_napster_data(self, data) -> dict:
        """
            Formated data following main class needs
            return: (dict) formatted data
        """
        try:
            infos = data['search']['data']['tracks'][0]
        except (KeyError, IndexError):
            raise NapsterError_TrackNotFound()
        return {
            'TIT2': infos['name'],
            'TPE1': infos['artistName'],
            'TALB': infos['albumName'],
            'TPE2': infos['name'],
            'TCON': self.__get_genres(infos),
            'album_cover': self.__get_album_cover_from_napster(infos)
        }

    def __get_album_cover_from_napster(self, infos: dict) -> str:
        """
            Extract album_id from infos and build cover's
            url before returning it
            return: (str) cover url
        """
        album_id = infos['albumId']
        return 'https://api.napster.com/imageserver/v2/albums/' \
            f'{album_id}/images/500x500.jpg'

    def __get_genres(self, infos: dict) -> list:
        """
            Fetch and return genres from Napster's API using genres's ids
            from fetched data
            return: (list) ['genre1', 'genre2']
        """
        genres_ids = ','.join(infos['links']['genres']['ids'])
        url = 'http://api.napster.com/v2.2/genres/' \
            f'{genres_ids}?apikey={NAPSTER_API_KEY}'
        data = self.__get_data_from_napster(url)
        return [genre['name'] for genre in data['genres']]
