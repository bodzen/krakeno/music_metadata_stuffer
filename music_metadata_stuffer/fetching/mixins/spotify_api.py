#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import NoReturn

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

from custom_py_exceptions import SubclassMissingVariableError
from sentry_python_integration.common import CustomSentryException

from music_metadata_stuffer.config import \
    SPOTIFY_CLIENT_ID, \
    SPOTIFY_CLIENT_SECRET


__all__ = [
    'spotify_api_mixin',
    'SpotifyError_InvalidCredentials',
    'SpotifyError_TooMuchRequest',
    'SpotifyError_TrackNotFound'
]


class SpotifyError_InvalidCredentials(CustomSentryException):
    pass


class SpotifyError_TooMuchRequest(CustomSentryException):
    pass


class SpotifyError_TrackNotFound(Exception):
    pass


class spotify_api_mixin:
    SPOTIPY = None

    def __init_subclass__(cls, *args, **kwargs):
        cls.__check_subclass_alpha(cls)
        super().__init_subclass__(*args, **kwargs)

    def __check_subclass_alpha(subclass) -> NoReturn:
        assert hasattr(subclass, 'title'), \
            SubclassMissingVariableError(subclass.__name__, 'title')
        pass

    def spotify_fetch_metadata(self) -> dict:
        """
            Request metadata to Spotify API using cropped title
            and return formated data to be used by main subclass
            return: (dict) formated data
        """
        self.__init_spotipy()
        data = self.__get_music_data_from_spotify()
        return self.__format_spotify_data(data)

    def __init_spotipy(self) -> NoReturn:
        """
            Init connection to spotify API
        """
        auth_manager = SpotifyClientCredentials(client_id=SPOTIFY_CLIENT_ID,
                                                client_secret=SPOTIFY_CLIENT_SECRET)
        self.SPOTIPY = spotipy.Spotify(auth_manager=auth_manager)

    def __get_music_data_from_spotify(self) -> dict:
        """
            Send request to spotify to fetch music's data
            return: (dict) music data
        """
        try:
            return self.SPOTIPY.search(self.title, limit=1)
        except spotipy.exceptions.SpotifyException as err:
            if err.http_status == 401:
                raise SpotifyError_InvalidCredentials()
            elif err.http_status == 429:
                raise SpotifyError_TooMuchRequest()
            else:
                raise err

    def __format_spotify_data(self, data: dict) -> dict:
        """
            Formated data following main class needs
            return: (dict) formatted data
        """
        try:
            infos = data['tracks']['items'][0]
        except (IndexError, KeyError):
            raise SpotifyError_TrackNotFound()
        return {
            'TIT2': infos.get('name'),
            'TPE1': self.__get_artists(infos),
            'TALB': self.__get_album_name(infos),
            'TPE2': self.__get_album_artists(infos),
            'TRCK': [infos.get('track_number')],
            'TDRC': self.__get_release_date(infos),
            'album_cover': self.__get_cover(infos)
        }

    def __get_artists(self, infos: dict) -> list:
        """
            Extract all artists' names from given data and return them as list
            return: (list) artists' names as string
        """
        return [artist['name'] for artist in infos['artists']]

    def __get_album_name(self, infos: dict) -> list:
        try:
            infos['album']['name']
        except KeyError:
            return None

    def __get_album_artists(self, infos: dict) -> list:
        """
            Extract all album's artists' names from given data and
            return them as list
            return: (list) album's artists' names as string
        """
        return [artist['name'] for artist in infos['album']['artists']]

    def __get_release_date(self, infos: dict) -> str:
        try:
            infos['album']['release_date']
        except KeyError:
            return None

    def __get_cover(self, infos: dict) -> str:
        try:
            return infos['album']['images'][0]['url']
        except (KeyError, IndexError):
            return None
