#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import NoReturn

import itunespy

from custom_py_exceptions import SubclassMissingVariableError


__all__ = [
    'itunes_api_mixin',
    'ItunesError_TrackNotFound'
]


class ItunesError_TrackNotFound(Exception):
    pass


class itunes_api_mixin:
    def __init_subclass__(cls, *args, **kwargs):
        cls.__check_subclass_beta(cls)
        super().__init_subclass__(*args, **kwargs)

    def __check_subclass_beta(subclass) -> NoReturn:
        assert hasattr(subclass, 'title'), \
            SubclassMissingVariableError(subclass.__name__, 'title')
        pass

    def itunes_fetch_metadata(self) -> dict:
        """
            Request metadata to Itunes Store API using cropped title
            and return formated data to be used by main subclass
            return: (dict) formated data
        """
        if not (data := self.__get_data_from_itunes()):
            raise ItunesError_TrackNotFound()
        return self.__format_itunes_data(data)

    def __get_data_from_itunes(self) -> dict:
        """
            Send request to itunes to fetch music's data
            return: (dict) music data
        """
        try:
            return itunespy.search(self.title, limit=1)
        except LookupError:
            raise ItunesError_TrackNotFound()

    def __format_itunes_data(self, data) -> dict:
        """
            Formated data following main class needs
            return: (dict) formatted data
        """
        try:
            infos = data[0].json
        except (IndexError):
            raise ItunesError_TrackNotFound()
        return {
            'TIT2': infos.get('trackName'),
            'TPE1': infos.get('artistName'),
            'TALB': infos.get('collectionName'),
            'TPE2': infos.get('artistName'),
            'TDRC': infos.get('releaseDate'),
            'TPOS': [infos.get('discNumber')],
            'TCON': infos.get('primaryGenreName'),
            'TRCK': [infos.get('trackNumber')],
            'album_cover': None,
        }
