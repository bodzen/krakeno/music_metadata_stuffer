#!/usr/bin/python3
# -*- coding: utf-8 -*-

from music_metadata_stuffer.fetching.mixins.napster_api import \
    napster_api_mixin, \
    NapsterError_TooMuchRequest, \
    NapsterError_TrackNotFound, \
    NapsterError_InvalidCredentials

from music_metadata_stuffer.fetching.mixins.spotify_api import \
    spotify_api_mixin, \
    SpotifyError_InvalidCredentials, \
    SpotifyError_TooMuchRequest, \
    SpotifyError_TrackNotFound

from music_metadata_stuffer.fetching.mixins.deezer_api import \
    deezer_api_mixin, DeezerError_TrackNotFound

from music_metadata_stuffer.fetching.mixins.itunes_store_api import \
    itunes_api_mixin, ItunesError_TrackNotFound
