#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
from typing import NoReturn

import requests
from acoustid import \
    fingerprint_file as get_fingerprint_and_duration

from music_metadata_stuffer.config import ACOUSTID_API_KEY


__all__ = [
    'audio_identifier',
    'AcoustidError_NoTrackFound'
]


class AcoustidError_NoTrackFound(Exception):
    pass


class audio_identifier:
    """
        Identify the title and artist of given audio file using
        acoustid services
    """
    def __init__(self, filepath: str, video_title: str) -> NoReturn:
        self.video_title = video_title.lower()
        self.filepath = filepath
        self.acoustif_base_url = self.__get_base_url()

    def __get_base_url(self) -> str:
        """
            Build and return Acoustid's API url, including authentification
            and type of infos to return.
            Fingerprint and meta still need to be substitue.
            return: (str) built url
        """
        requested_infos = '+'.join(('recordings', 'recordingids',
                                    'releases', 'releaseids',
                                    'releasegroups', 'releasegroupids',
                                    'tracks', 'compress'))
        return 'https://api.acoustid.org/v2/lookup' \
            f'?client={ACOUSTID_API_KEY}&meta={requested_infos}' \
            '&duration={duration}&fingerprint={fingerprint}'

    def get_track_title(self) -> str:
        """
            Request infos from acoustid api using the audio file's fingerprint
            return: (str) '{artist's name} {track's name}'
        """
        duration, fingerprint = get_fingerprint_and_duration(self.filepath)
        raw_data = self.__request_track_infos(int(duration), fingerprint)
        return self.__get_track_title(raw_data)

    def __request_track_infos(self, duration: int, fingerprint: bytes) -> dict:
        """
            Request track's infos to Acoustid's API.
            return: (dict) raw infos returned by API
        """
        decoded_fingerprint = fingerprint.decode()
        url = self.acoustif_base_url.format(duration=duration,
                                            fingerprint=decoded_fingerprint)
        resp = requests.post(url)
        return self.__get_raw_data(resp)

    def __get_raw_data(self, resp):
        try:
            raw_data = resp.json()
        except json.decoder.JSONDecodeError:
            raise AcoustidError_NoTrackFound()

        if not len(raw_data['results']):
            raise AcoustidError_NoTrackFound()
        return raw_data

    def __get_track_title(self, raw_data: dict, i_record=0) -> str:
        """
            Extract track's name and first artist from given data,
            and build the track title before returning it.
            return: (str) track's title '{artist's name} {track's name}'
        """
        try:
            basic_infos = raw_data['results'][0]['recordings'][i_record]
        except IndexError:
            raise AcoustidError_NoTrackFound()
        if 'title' in basic_infos:
            track_infos = self.__get_basic_infos(basic_infos)
        else:
            track_infos = self.__get_nested_infos(basic_infos)
        if track_infos['title'].lower() not in self.video_title and \
           track_infos['artist'].lower() not in self.video_title:
            return self.__get_track_title(raw_data, i_record + 1)
        return track_infos['artist'] + ' ' + track_infos['title']

    def __get_nested_infos(self, basic_infos: dict) -> dict:
        infos = basic_infos['releasegroups'][0]['releases'][0]['mediums'][0]
        return {
            'track_count': infos.get('track_count'),
            'position': infos['tracks'][0].get('position'),
            'artist': infos['tracks'][0]['artists'][0]['name'],
            'title': infos['tracks'][0]['title']
        }

    def __get_basic_infos(self, basic_infos: dict) -> dict:
        return {
            'title': basic_infos.get('title'),
            'artist': basic_infos['artists'][0]['name'],
            'position': None,
            'track_count': None
        }
