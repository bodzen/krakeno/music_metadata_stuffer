#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name='music_metadata_stuffer',
    version='1.0.0',
    author='dh4rm4',
    description='Fetch metadata using a given video title via Music Streaming services\' API and '
    'fill given file with those.',
    install_requires=[
        'pyacoustid==1.2.0',
        'requests==2.20.0',
        'spotipy==2.16.0',
        'deezer-python==2.2.1',
        'itunespy==1.6.0',
        'mutagen==1.45.1',
        'custom_py_exceptions @ git+ssh://git@gitlab.com/bodzen/custom_py_exceptions.git#egg=custom_py_exceptions',
        'sentry_python_integration @ git+ssh://git@gitlab.com/bodzen/common/sentry_python_integration@omega-13#egg=sentry_python_integration',
    ],
    classifiers=[
        "Programming Language :: Python 3.8+",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
