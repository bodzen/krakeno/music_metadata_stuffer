# Music Metadata Stuffer

Fetch metadata using a given video title via Music Streaming services' API.


## How to

1. Be sure to add library in requirements.txt
```
-e git+ssh://git@gitlab.com/bodzen/krakeno/music_metadata_stuffer@omega-X#egg=music_metadata_stuffer
```
2. Import and use
```
from music_metadata_stuffer import metadata_stuffer
metadata_stuffer()
```
